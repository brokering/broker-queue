#ifndef INCLUDED_SUPPORTED_DATATYPES
#define INCLUDED_SUPPORTED_DATATYPES

// Add new data types for queuing below
class LargerStruct
{
public:
	__device__ LargerStruct(int x) : a(x), c(x)
	{
	}

	__device__ LargerStruct() : a(0), c(0)
	{
	}

private:
	int a;
	unsigned int c;
};

#endif

