#include "helper.cuh"

template <typename Q>
__global__ void sizeOnDevice(unsigned int* deviceSize)
{
	*deviceSize = sizeof(Q);
}

template <typename Q>
__global__ void placeQueue(void* deviceQueueP)
{
	new (deviceQueueP) Q();
}

template <typename Q>
__global__ void initTest(Q* queueP)
{
	Q& queue = *queueP;

	queue.init();
}

template <typename Q, typename T>
__global__ void launchTest(Q* queueP, unsigned int numIterations, float enqProb, float deqProb, bool waitForData)
{
	Q& queue = *queueP;

	int lid = threadIdx.x + blockIdx.x * blockDim.x;
	
	T a(lid), b;

	float rand_val;
	unsigned int rng_state = lid;

	for (int i = 0; i < numIterations; i++)
	{
		rand_val = rand_lcg(rng_state);
		if (rand_val <= enqProb)
		{
			queue.enqueue(a);
		}

		rand_val = rand_lcg(rng_state);
		if (rand_val <= deqProb)
		{
			bool hasData = false;
			queue.dequeue(hasData, b);
			while (waitForData && !hasData)
			{
				queue.dequeue(hasData, b);
			}
		}
	}
}